function add_universal_path
    if not contains $argv $fish_user_paths
        set --universal fish_user_paths $fish_user_paths $argv
    end
end

add_universal_path /usr/local/bin $HOME/.local/bin

set --export --global EDITOR nvim
set --export --global LANG en_US.UTF-8

#######################################################
#                                                     #
#  One Half Dark with Yellow Jelly Beans              #
#                                                     #
#######################################################

set --local black 282c34
set --local dark_gray 363c47
set --local medium_gray 4d5666
set --local light_gray 5d677a
set --local white dcdfe4

set --local red e06c75
set --local green 98c379
set --local yellow fad07a
set --local blue 61afef
set --local magenta c678dd
set --local cyan 56b6c2


#######################################################
#                                                     #
#  Fish                                               #
#                                                     #
#######################################################

set fish_color_normal $white
set fish_color_command $white --bold
set fish_color_quote $yellow
set fish_color_redirection $cyan --bold
set fish_color_end $white --bold
set fish_color_error $red
set fish_color_param $blue --bold
set fish_color_comment $light_gray
set fish_color_match $magenta
set fish_color_selection --background=$light_gray
set fish_color_search_match $cyan --bold
set fish_color_operator $magenta --bold
set fish_color_escape $red --bold
set fish_color_autosuggestion $yellow

set --global fish_prompt_pwd_dir_length 0
set --global fish_key_bindings fish_hybrid_key_bindings


#######################################################
#                                                     #
#  asdf                                               #
#                                                     #
#######################################################

source ~/.asdf/asdf.fish


#######################################################
#                                                     #
#  bobthefish                                         #
#                                                     #
#######################################################

set --global theme_powerline_fonts no
set --global theme_display_git_master_branch yes
set --global theme_display_git_untracked no


#######################################################
#                                                     #
#  Bat                                                #
#                                                     #
#######################################################

set --export --global BAT_STYLE full
set --export --global BAT_THEME OneHalfDark


#######################################################
#                                                     #
#  fzf                                                #
#                                                     #
#######################################################

if test -d $HOME/.fzf/bin
    add_universal_path $HOME/.fzf/bin
end

if test (which fzf)
    set --export --global FZF_DEFAULT_OPTS "--layout=reverse
                                               --height=40%
                                               --info=inline
                                               --multi
                                               --color=fg:#c3c6cc,bg:#$black,hl:#$cyan
                                               --color=fg+:#$yellow,bg+:#$medium_gray,hl+:#$cyan
                                               --color=info:#$red,prompt:#$blue,pointer:#$yellow
                                               --color=marker:#$red,spinner:#$blue,header:#$cyan"

    if test (which rg)
        set --export --global FZF_DEFAULT_COMMAND 'rg --files --smart-case --no-ignore-vcs --hidden --follow --glob "!.git/*"'
        set --export --global FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"
    else if test (which fd)
        set --export --global FZF_DEFAULT_COMMAND 'fd --type file --no-ignore-vcs --hidden --follow'
        set --export --global FZF_CTRL_T_COMMAND "$FZF_DEFAULT_COMMAND"
    end
end


#######################################################
#                                                     #
#  Atuin                                              #
#                                                     #
#######################################################

if test (which atuin)
    # Setup Atuin after fzf to override fzf Ctrl-r key binding
    atuin init fish --disable-up-arrow | source
end

#######################################################
#                                                     #
#  Neovim                                             #
#                                                     #
#######################################################

if test -d $HOME/.local/neovim/bin
    add_universal_path $HOME/.local/neovim/bin
end


#######################################################
#                                                     #
#  Stow                                               #
#                                                     #
#######################################################

set --export --global STOW_DIR $HOME/.home-files


#######################################################
#                                                     #
#  Rust                                               #
#                                                     #
#######################################################

if test -d $HOME/.cargo/bin
    add_universal_path $HOME/.cargo/bin
end


#######################################################
#                                                     #
#  zoxide                                             #
#                                                     #
#######################################################

if test (which zoxide)
    zoxide init --cmd cd fish | source
end


#######################################################
#                                                     #
#  global NPM packages directory                      #
#                                                     #
#######################################################

if test (which node)
    set global_npm_packages_dir "$HOME/.npm-global-packages"

    if ! test -d $global_npm_packages_dir
        mkdir $global_npm_packages_dir
    end

    add_universal_path "$global_npm_packages_dir/bin"
    npm config --global set prefix $global_npm_packages_dir
end


#######################################################
#                                                     #
#  Go                                                 #
#                                                     #
#######################################################

if test -d /usr/local/go/bin
    add_universal_path /usr/local/go/bin

    set --export --global GDK_GO_VERSION 1.12
end


#######################################################
#                                                     #
#  Docker                                             #
#                                                     #
#######################################################

if test -d $HOME/.docker/bin
    add_universal_path $HOME/.docker/bin
end


#######################################################
#                                                     #
#  Android                                            #
#                                                     #
#######################################################

if test -d $HOME/android
    add_universal_path $HOME/android/tools
    add_universal_path $HOME/android/build-tools

    set --export --global ANDROID_HOME $HOME/android
end


#######################################################
#                                                     #
#  Java                                               #
#                                                     #
#######################################################

if test -f /usr/lib/jvm/java-8-openjdk-amd64
    add_universal_path /usr/lib/jvm/java-8-openjdk-amd64
end


#######################################################
#                                                     #
#  local.config.fish                                  #
#                                                     #
#######################################################

if test -f $HOME/.config/fish/local.config.fish
    source $HOME/.config/fish/local.config.fish
else if test -f $HOME/.config/fish/local.config.fish.template
    cp $HOME/.config/fish/local.config.fish.template $HOME/.config/fish/local.config.fish
end


#######################################################
#                                                     #
#  setup pair programmer files                        #
#                                                     #
#######################################################

# if ! test -d $HOME/.vim/.pair-programmers
#   mkdir $HOME/.vim/.pair-programmers
#   touch $HOME/.vim/.pair-programmers/.co-authors
#   touch $HOME/.vim/.pair-programmers/.usernames
# end
