function ll
  if exa --version > /dev/null 2>&1
    exa --long --icons --all --group --header --octal-permissions $argv
  else if ls --version > /dev/null 2>&1
    ls -l --almost-all --human-readable $argv
  else
    ls -lah $argv
  end
end
