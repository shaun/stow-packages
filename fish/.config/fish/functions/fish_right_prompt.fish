function fish_right_prompt
  set -l mono0 282c34
  set -l mono1 313640
  set -l mono2 5d677a
  set -l mono3 dcdfe4

  if test $COLUMNS -gt 100
    set_color --background $mono2
    set_color $mono3

    echo -n (date '+ %T %Z ')
  end

  if test $COLUMNS -gt 140
    set_color --background $mono3
    set_color $mono1

    echo -n ' '
    echo -n (whoami)
    echo -n ' '
  end

  set_color normal
end
