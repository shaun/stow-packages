function bobthefish_colors -S -d 'Define a custom bobthefish color scheme'
  set --local black      282c34
  set --local dark_gray  363c47
  set --local light_gray 5d677a
  set --local white      dcdfe4

  set --local red     e06c75
  set --local green   98c379
  set --local yellow  fad07a
  set --local blue    61afef
  set --local magenta c678dd
  set --local cyan    56b6c2

  set -x color_initial_segment_exit     $dark_gray $red --bold
  set -x color_initial_segment_su       $dark_gray $green --bold
  set -x color_initial_segment_jobs     $dark_gray $blue --bold

  set -x color_path                     $light_gray $white
  set -x color_path_basename            $light_gray $white
  set -x color_path_nowrite             $black $red
  set -x color_path_nowrite_basename    $black $red --bold

  set -x color_repo                     $white $light_gray --bold
  set -x color_repo_work_tree           $white $light_gray --bold
  set -x color_repo_dirty               $magenta $dark_gray
  set -x color_repo_staged              $blue $dark_gray

  set -x color_vi_mode_default          $magenta $black
  set -x color_vi_mode_insert           $white $light_gray --bold
  set -x color_vi_mode_visual           $yellow $black --bold

  set -x color_vagrant                  $cyan $dark_gray --bold
  set -x color_k8s                      $green $dark_gray --bold
  set -x color_username                 $dark_gray $blue --bold
  set -x color_hostname                 $dark_gray $blue
  set -x color_rvm                      $blue $dark_gray --bold
  set -x color_nvm                      $blue $dark_gray --bold
  set -x color_virtualfish              $blue $dark_gray --bold
  set -x color_virtualgo                $blue $dark_gray --bold
  set -x color_desk                     $blue $dark_gray --bold
end
