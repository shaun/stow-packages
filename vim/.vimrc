if &shell =~# 'fish$'
  set shell=/bin/bash
endif

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/bundle')

Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/benmills/vimux.git'
Plug 'https://github.com/christoomey/vim-tmux-navigator.git'
Plug 'https://github.com/dense-analysis/ale.git'
Plug 'https://github.com/easymotion/vim-easymotion.git'
Plug 'https://github.com/itchyny/lightline.vim.git'
Plug 'https://github.com/janko/vim-test.git'
Plug 'https://github.com/jeetsukumaran/vim-buffergator.git'
Plug 'https://github.com/jistr/vim-nerdtree-tabs.git'
Plug 'https://github.com/joshdick/onedark.vim.git'
Plug 'https://github.com/junegunn/fzf.git', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/junegunn/gv.vim.git'
Plug 'https://github.com/junegunn/vim-easy-align'
Plug 'https://github.com/junegunn/vim-emoji.git'
Plug 'https://github.com/majutsushi/tagbar.git', { 'on': 'TagbarToggle' }
Plug 'https://github.com/mbbill/undotree.git'
Plug 'https://github.com/milkypostman/vim-togglelist.git'
Plug 'https://github.com/rhysd/git-messenger.vim.git'
Plug 'https://github.com/rhysd/reply.vim.git'
Plug 'https://github.com/scrooloose/nerdtree.git'
Plug 'https://github.com/sheerun/vim-polyglot.git'
Plug 'https://github.com/tpope/vim-abolish.git'
Plug 'https://github.com/tpope/vim-commentary.git'
Plug 'https://github.com/tpope/vim-dispatch.git'
Plug 'https://github.com/tpope/vim-endwise.git'
Plug 'https://github.com/tpope/vim-eunuch.git'
Plug 'https://github.com/tpope/vim-fugitive.git'
Plug 'https://github.com/tpope/vim-repeat.git'
Plug 'https://github.com/tpope/vim-sensible.git'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/tpope/vim-unimpaired.git'
Plug 'https://github.com/wincent/ferret.git'
Plug 'https://github.com/vim-scripts/YankRing.vim.git'

" ruby plugins
Plug 'https://github.com/kana/vim-textobj-user.git',          { 'for': 'ruby' }
Plug 'https://github.com/lucapette/vim-ruby-doc.git',         { 'for': 'ruby' }
Plug 'https://github.com/nelstrom/vim-textobj-rubyblock.git', { 'for': 'ruby' }
Plug 'https://github.com/tpope/vim-rails.git',                { 'for': 'ruby' }
Plug 'https://github.com/vim-scripts/ruby-matchit.git',       { 'for': 'ruby' }

" rust plugins
Plug 'https://github.com/rust-lang/rust.vim.git', { 'for': 'rust' }

" vim plugins
Plug 'https://github.com/guns/xterm-color-table.vim.git', { 'for': 'vim' }
Plug 'https://github.com/edkolev/tmuxline.vim.git',       { 'for': 'vim' }

call plug#end()

syntax on                         " Turn on syntax highlighting
set encoding=utf-8

set hidden                        " Keep modified buffers (without saving them) when they are hidden/abandoned

set wildmode=list:full            " List out auto-complete matches and use tab to cycle through them

set ignorecase                    " Turn on case-insensitive searching
set smartcase                     " Override ignorecase if search pattern contains upper case characters
set hlsearch                      " Highlight search matches

set number                        " Show line numbers
set cursorline                    " Highlight the line with the cursor

set wrap                          " Turn on line wrapping
set linebreak                     " Prevent line breaks in the middle of words
set scrolloff=3                   " Set number of lines to keep around the cursor

set title                         " Set the terminal's title

set visualbell                    " Use a visual bell instead of beeping

set history=200                   " Set number of lines of history that are remembered
set undolevels=500                " Set number of changes that can be undone

if has('persistent_undo')
  set undofile
  set undodir=~/.vim/.undo

  if !isdirectory(&undodir)
    call mkdir(&undodir, 'p')
  endif
endif

" Don't make backups at all
set nobackup
set nowritebackup
set noswapfile

set smartindent                   " Turn on automatic indenting
set tabstop=2                     " Global tab width.
set shiftwidth=2                  " And again, related.
set softtabstop=2
set expandtab                     " Use spaces instead of tabs

set noshowmode                    " Hide the default mode text (e.g. -- INSERT -- below the statusline)

" enable mouse in all modes
set mouse=a

" split windows below/right
set splitbelow
set splitright

" tab and end-of-line characters
set listchars=tab:▸\ ,eol:¬

set updatetime=100

" by default <\> is the leader key and <,> is the reverse charcter search key
" let's reverse them
let mapleader = ','
noremap \ ,

" shortcuts to insert debugging calls
nnoremap <silent><leader>dc iconsole.log();<esc>F(a
nnoremap <silent><leader>dp ibinding.pry<esc>
nnoremap <silent><leader>dr iRails.logger.info 'asdf: '<esc>i

" shortcut to edit vimrc
nnoremap <silent><leader>ev :execute 'edit ' . resolve(expand($MYVIMRC))<CR>

nnoremap <silent><leader>ff :GFiles<CR>
nnoremap <silent><leader>fF :FZF<CR>
nnoremap <silent><leader>fb :Buffers<CR>
nnoremap <silent><leader>fh :Helptags<CR>

" -----------------------------------------------
" ** - regular * search
" *r - ripgrep search the word under cursor
" *c - jump to first ctags tag match
" -----------------------------------------------

nnoremap ** *
nnoremap *r :Rg <c-r><c-w><CR>
nnoremap *c g<C-]>

" shortcut to start a ripgrep search
nnoremap <leader>rg :Rg<Space>
nnoremap <leader>rG :RG<Space>

" don't move when searching for word under cursor
nnoremap * *<c-o>

" map the tab/shift-tab to indent/outdent and then reselect
vnoremap <Tab> >gv
vnoremap <S-Tab> <gv

nnoremap <silent><space> :call utilities#CloseAllTheThings()<CR>

" save/write files w/ control-k
noremap <C-k> :write<CR>
inoremap <C-k> <ESC>:write<CR>

" map jk (in insert mode) to go to normal mode
inoremap jk <ESC>

" make it so that when you up/down on wrapped lines, it goes to the next row...
nnoremap j gj
nnoremap k gk

nnoremap td :bd<CR>

" go prev/next buffers
nnoremap th :bp<CR>
nnoremap tl :bn<CR>

" toggle back and forth between last/current buffer
nnoremap tt :b#<CR>

" make Y behave like C/D
nnoremap Y y$

" -----------------------------------------------
"   One Dark with Yellow Jelly Beans
" -----------------------------------------------

let g:black      = { 'gui': '#282c34', 'cterm': '236' }
let g:dark_gray  = { 'gui': '#363c47', 'cterm': '237' }
let g:light_gray = { 'gui': '#5d677a', 'cterm': '243' }
let g:white      = { 'gui': '#dcdfe4', 'cterm': '188' }

let g:red        = { 'gui': '#e06c75', 'cterm': '168' }
let g:green      = { 'gui': '#98c379', 'cterm': '114' }
let g:yellow     = { 'gui': '#fad07a', 'cterm': '180' }
let g:blue       = { 'gui': '#61afef', 'cterm': '75'  }
let g:magenta    = { 'gui': '#c678dd', 'cterm': '176' }
let g:cyan       = { 'gui': '#56b6c2', 'cterm': '73'  }


" -----------------------------------------------
"   Theme settings
" -----------------------------------------------

set t_Co=256

if has('termguicolors')
  set termguicolors
endif

let g:onedark_color_overrides = {
\ "yellow": {"gui": g:yellow.gui, "cterm": g:yellow.cterm, "cterm16": "0" },
\}

if (has("autocmd"))
  augroup colorextend
    autocmd!
    autocmd ColorScheme * call onedark#extend_highlight("CursorLine", { "bg": { "gui": g:dark_gray.gui, "cterm": g:dark_gray.cterm } })
    autocmd ColorScheme * call onedark#extend_highlight("gitcommitSummary", { "fg": { "gui": g:blue.gui, "cterm": g:blue.cterm } })
  augroup END
endif

colorscheme onedark


" -----------------------------------------------
"   Git Shortcuts
" -----------------------------------------------

let g:gitgutter_map_keys = 0

" $ git add --patch
nnoremap <leader>ga :GitGutterStageHunk<CR>

" $ git blame
nnoremap <leader>gb :Git blame<CR>

" git diff/preview hunk
nnoremap <leader>gd :GitGutterPreviewHunk<CR> :wincmd P<CR> :resize +3<CR> :echo ''<CR>

" git log for file
nnoremap <leader>glf :call git#GitLogForFile()<CR>

" git log
nnoremap <leader>gll :call git#GitLog()<CR>

" git log for current line
nnoremap <leader>gln :GitMessenger<CR>

" go to next hunk
nmap <leader>gn <Plug>(GitGutterNextHunk)

" go to previous hunk
nmap <leader>gp <Plug>(GitGutterPrevHunk)

" $ git status
nnoremap <leader>gs :Gstatus<CR> :echo 'g?: help / c?: commit help / gq: quit'<CR>

" $ git checkout --patch
nnoremap <leader>gu :GitGutterUndoHunk<CR>



" -----------------------------------------------
"   Toggle Shortcuts
" -----------------------------------------------

" toggle buffergator
nnoremap <leader>tb :BuffergatorToggle<CR>

" toggle ctags tagbar
nnoremap <leader>tc :TagbarToggle<CR>

" toggle git gutter
nnoremap <leader>tgg :GitGutterToggle<CR>

" toggle git gutter folding
nnoremap <leader>tgf :GitGutterFold<CR>

" toggle nerdtree
nnoremap <leader>tn :NERDTreeMirrorToggle<CR>

" toggle quickfix list
nnoremap <silent><leader>tq :call ToggleQuickfixList()<CR>

" toggle spell-check
nnoremap <leader>tS :call utilities#ToggleSpellCheck()<CR>

" toggle undo-tree
nnoremap <leader>tu :UndotreeToggle<CR>

" toggle yankring
nnoremap <leader>ty :YRShow<CR>



" -----------------------------------------------
"   ALE
" -----------------------------------------------

let g:ale_sign_error   = emoji#for('ghost')
let g:ale_sign_warning = emoji#for('jack_o_lantern')

highlight clear ALEErrorSign
highlight clear ALEWarningSign


" -----------------------------------------------
"   Buffergator
" -----------------------------------------------

let g:buffergator_autoexpand_on_split   = 0
let g:buffergator_sort_regime           = "mru"
let g:buffergator_split_size            = 10
let g:buffergator_suppress_keymaps      = 1
let g:buffergator_viewport_split_policy = "B"


" -----------------------------------------------
"  easymotion
" -----------------------------------------------

let g:EasyMotion_keys = 'abcdefghijklmnopqrstuvwxyz;'


" -----------------------------------------------
"  Ferret
" -----------------------------------------------

let g:FerretMap = 0

" -----------------------------------------------
"   git-messenger.vim
" -----------------------------------------------

let g:git_messenger_always_into_popup   = v:true
let g:git_messenger_include_diff        = "all"
let g:git_messenger_max_popup_height    = 15
let g:git_messenger_no_default_mappings = v:true


" -----------------------------------------------
"   lightline
" -----------------------------------------------

let g:lightline = {
      \ 'colorscheme': 'onedarkcustom',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'relativepath', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'fugitive#head'
      \ },
      \ }


" -----------------------------------------------
"   NERDTree
" -----------------------------------------------

let NERDTreeAutoDeleteBuffer = 1
let NERDTreeChDirMode        = 2
let NERDTreeMinimalUI        = 1
let NERDTreeQuitOnOpen       = 1
let NERDTreeShowBookmarks    = 1
let NERDTreeShowHidden       = 1


" -----------------------------------------------
"   NERDTree Tabs
" -----------------------------------------------
let g:nerdtree_tabs_open_on_gui_startup = 0


" -----------------------------------------------
"   reply.vim
" -----------------------------------------------

nnoremap <silent><leader>ep vip:ReplSend<CR>
vnoremap <silent><leader>ep :ReplSend<CR>


" -----------------------------------------------
"   tmuxline
" -----------------------------------------------

let g:tmuxline_powerline_separators = 0
let g:tmuxline_preset = {
      \ 'a'    : '٩(๑•◡-•)つ─◦*✿ #S',
      \ 'b'    : '',
      \ 'c'    : '',
      \ 'win'  : ' #I #W',
      \ 'cwin' : '#W✧◦*°◝(*❛ᴗ❛)ﾉ°◦✧ #W',
      \ 'x'    : '',
      \ 'y'    : '#(date "+%a %d-%b-%Y")',
      \ 'z'    : '#H'
      \ }

let g:tmuxline_theme = {
      \ 'a'    : [ g:dark_gray.gui, g:white.gui ],
      \ 'b'    : [ g:white.gui, g:light_gray.gui ],
      \ 'c'    : [ g:white.gui, g:black.gui ],
      \ 'win'  : [ g:white.gui, g:light_gray.gui ],
      \ 'cwin' : [ g:black.gui, g:blue.gui],
      \ 'x'    : [ g:white.gui, g:black.gui ],
      \ 'y'    : [ g:white.gui, g:light_gray.gui ],
      \ 'z'    : [ g:dark_gray.gui, g:white.gui ],
      \ 'bg'   : [ g:white.gui, g:dark_gray.gui ],
      \ }


" -----------------------------------------------
"   undo-tree
" -----------------------------------------------

let g:undotree_SetFocusWhenToggle = 1
let g:undotree_WindowLayout       = 2


" -----------------------------------------------
"   vim-easy-align
" -----------------------------------------------
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" -----------------------------------------------
"   vim-test
" -----------------------------------------------

let test#ruby#rspec#options = '--format documentation'
let test#strategy           = 'vimux'

nnoremap <silent><leader>rr :TestFile<CR>
nnoremap <silent><leader>re :TestSuite<CR>
nnoremap <silent><leader>rl :TestLast<CR>
nnoremap <silent><leader>rf :TestVisit<CR>


" -----------------------------------------------
"   Vim Tmux Navigator
" -----------------------------------------------

let g:tmux_navigator_no_mappings = 1

nnoremap <silent><leader>0l :TmuxNavigateLeft<CR>
nnoremap <silent><leader>0d :TmuxNavigateDown<CR>
nnoremap <silent><leader>0u :TmuxNavigateUp<CR>
nnoremap <silent><leader>0r :TmuxNavigateRight<CR>


" -----------------------------------------------
"   vimux
" -----------------------------------------------

let g:VimuxHeight      = '40'
let g:VimuxOrientation = 'h'


" -----------------------------------------------
"   YankRing settings
" -----------------------------------------------

let g:yankring_history_dir = '~/.vim'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MULTIPURPOSE TAB KEY
" Indent if we're at the beginning of a line. Else, do completion.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col
        return "\<tab>"
    endif

    let char = getline('.')[col - 1]
    if char =~ '\k'
        " There's an identifier before the cursor, so complete the identifier.
        return "\<c-p>"
    else
        return "\<tab>"
    endif
endfunction
inoremap <expr> <tab> InsertTabWrapper()
inoremap <s-tab> <c-n>

function! MapCR()
  nnoremap <CR> :TestNearest<CR>
endfunction

call MapCR()

function! RandomEmoticonTracer()
let emoticons = [
      \ '"ε=ε=ε=ε=ε=ε=ε=ε=ε=ε=ε=ε=ε==┌(`;@@)┘"',
      \ '"─────────────────==≡≡ΣΣ((( つºل͜º)つ"',
      \ '"───────────────────===≡≡ΣΣ(> ^_^ )>"',
      \ '"ヽ(＾▽＾)ﾉ┌┛Σ≡≡＝＝＝＝＝＝＝＝＝＝"',
      \ '"♪ヽ( ･益･)ﾉ┌┛ﾟ･*:.｡................"',
      \ '"ᕦ[ •́ ﹏ •̀ ]⊃¤=[]:::::::::::::::::::"',
      \ '"╰༼.◕ヮ◕.༽つ¤=[]————————————————————"',
      \ '"( つ•̀ω•́)つ・・*:・:・:====≡≡≡≡≡≡≡≡Σ"',
      \ '"(っ・ω・）っ≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡≡☆"',
      \ '"(┛｀Д´)┛・…‥‥炎炎炎炎炎炎炎炎炎炎炎"',
      \ '"(⊃｡•́‿•̀｡)⊃━━━✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿✿"',
      \ '"(・ω・)＝《《≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠≠〇"',
      \ '"╰(✿´⌣`✿)╯♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡♡"',
      \ '"(ﾉ´ヮ´)ﾉ*:･ﾟ✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧"',
      \ '"（｡･ω･｡)つ━☆・*********************"',
      \ '"（< ˘³˘）―━━☆⌒♪♫♪♫♪♪♫♪♫♪♫♪♫♪♫♪♫♪♫♫♪"']

  return emoticons[s:randnum(len(emoticons))]
endfunction

function! s:randnum(max) abort
  return str2nr(matchstr(reltimestr(reltime()), '\v\.@<=\d+')[1:]) % a:max
endfunction

iabbr ", <C-R>=RandomEmoticonTracer()<CR><LEFT>
