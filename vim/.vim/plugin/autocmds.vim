if has('autocmd')
  augroup dynamic_cursorline
    autocmd!
    autocmd InsertLeave,WinEnter * set cursorline
    autocmd InsertEnter,WinLeave * set nocursorline
  augroup END

  augroup highlight_extra_whitespace
    execute "highlight ExtraWhitespace" . " ctermbg=" . g:magenta.cterm . " guibg=" . g:magenta.gui
    match ExtraWhitespace /\s\+$/

    autocmd!
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()

    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/

    autocmd FileType GV match ExtraWhitespace //
  augroup END

  augroup map_cr
    autocmd!
    autocmd CmdwinEnter *        :unmap <cr>
    autocmd CmdwinLeave *        :call MapCR()
    autocmd BufReadPost quickfix nnoremap <buffer><cr> <cr>
  augroup END

  augroup restore_last_cursor_position
    autocmd!
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal! g'\"" |
    \ endif

    autocmd BufReadPost COMMIT_EDITMSG execute "normal! gg" | startinsert
  augroup END
endif
