function! utilities#FormatParagraph()
  :normal! gqip
endfunction

function! utilities#ToggleSpellCheck()
  if &spell ==? 'nospell'
    set spell
    set complete+=kspell
    echo "Spell-check with KSpell enabled"
  else
    set nospell
    set complete-=kspell
    echo "Spell-check with KSpell disabled"
  endif
endfunction

function! utilities#CloseAllTheThings()
  :cclose
  :pclose
  :NERDTreeClose
  :BuffergatorClose
  :GitMessengerClose
  :silent ReplStop
  :silent! TagbarClose
  :UndotreeHide
  :VimuxCloseRunner

  for wnr in range(1, winnr('$'))
    if !empty(getwinvar(wnr, 'fugitive_leave'))
      execute winbufnr(wnr).'bdelete'
    endif
  endfor

  if &filetype ==? 'vim-plug'
    q
  endif
endfunction
