function! javascript#AutoFixLintingIssuesFor(linters)
  let b:ale_fixers = a:linters

  :ALEFix
endfunction
