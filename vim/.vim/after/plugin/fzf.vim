command! -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color always --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview({'options': '--height 100%'}, 'right:50%'), 1)

command! -nargs=* RG
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color always --no-ignore-vcs --hidden --glob "!.git/*" --smart-case '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview({'options': '--height 100%'}, 'right:50%'), 1)
