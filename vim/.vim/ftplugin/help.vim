" Shortcuts when viewing Vim help:
"
" <ENTER>     - Navigate to the topic under the cursor
" <BACKSPACE> - Navigate back
" <q>         - Quit help

nnoremap <buffer><cr> <c-]>
nnoremap <buffer><bs> <c-t>
nnoremap <buffer>q    :bdelete!<cr>
