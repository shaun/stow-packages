nnoremap <silent><buffer><leader>gh :tab help <c-r><c-w><cr>
nnoremap <silent><buffer><leader>pi :PlugInstall<cr>
nnoremap <silent><buffer><leader>pu :PlugUpdate<cr>
nnoremap <silent><buffer><leader>pc :PlugClean<cr>
nnoremap <silent><buffer><leader>sv :source $MYVIMRC<cr> :echo "Reloaded .vimrc!"<cr>
