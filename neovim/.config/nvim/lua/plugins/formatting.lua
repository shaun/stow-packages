return {
  "https://github.com/stevearc/conform.nvim.git",

  -- These settings will get merged with LazyVim's settings
  opts = {
    formatters_by_ft = {
      ["*"] = { "trim_whitespace" },
      lua = { "stylua" },
      elixir = { "mix" },
      go = { "gofmt" },
      ruby = { "standardrb" },
      rust = { "rustfmt" },
      javascript = { "prettierd", "prettier", stop_after_first = true },
      typescript = { "prettierd", "prettier", stop_after_first = true },
      css = { "prettierd", "prettier", stop_after_first = true },
      scss = { "prettierd", "prettier", stop_after_first = true },
      html = { "prettierd", "prettier", stop_after_first = true },
      json = { "prettierd", "prettier", stop_after_first = true },
      eruby = { "prettierd", "prettier", stop_after_first = true },
    },
  },
}
