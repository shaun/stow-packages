return {
  -----------------------------------------------------------------------------
  -- mason - manage LSP servers, DAP servers, linters, and formatters
  -----------------------------------------------------------------------------
  {
    "https://github.com/williamboman/mason.nvim.git",

    -- These settings will get merged with LazyVim's settings
    opts = {
      ui = {
        check_outdated_packages_on_open = true, -- Automatically check for new versions when opening the :Mason window
        border = "single",
      },
    },

    --  Disable all keymappings set by LazyVim
    keys = false,
  },

  ------------------------------------------------------------------------------
  -- persisted - session manager
  ------------------------------------------------------------------------------
  {
    "https://github.com/olimorris/persisted.nvim.git",

    opts = {
      autoload = true, -- Automatically load the last session
      use_git_branch = true, -- Create sessions based on git branch names
      should_autosave = function()
        -- Don't autosave when on the dashboard, for new/blank files, or for gitcommit files
        if vim.bo.filetype == "dashboard" or vim.bo.filetype == "" or vim.bo.filetype == "gitcommit" then
          return false
        end

        return true
      end,

      telescope = {
        before_source = function()
          if vim.bo.filetype ~= "dashboard" then
            vim.cmd("SessionSave") -- Save the current session before loading a new session
          end

          vim.api.nvim_input("<ESC>:%bd<CR>") -- Close all open buffers before loading a new session
        end,

        after_source = function(session)
          local message = "Successfully loaded session: " .. session.name

          vim.defer_fn(function()
            vim.notify(message, vim.log.levels.INFO, { title = "Session manager" })
          end, 0)
        end,
      },
    },
  },

  -----------------------------------------------------------------------------
  -- persistence - simple session management to restore last session
  -----------------------------------------------------------------------------
  {
    "https://github.com/folke/persistence.nvim.git",

    event = "BufReadPre", -- Start session-saving after a file has been opened

    -- These settings will get merged with LazyVim's settings
    opts = {
      dir = vim.fn.expand(vim.fn.stdpath("state") .. "/persistence-sessions/"),
    },

    --  Disable all keymappings set by LazyVim
    keys = false,
  },
}
