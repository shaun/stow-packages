return {
  { "https://github.com/rust-lang/rust.vim.git", ft = "rust" },
  { "https://github.com/simrat39/rust-tools.nvim.git", ft = "rust" },
}
