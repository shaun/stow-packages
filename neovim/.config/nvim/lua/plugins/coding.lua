return {

  -----------------------------------------------------------------------------
  -- LuaSnip - snippets engine
  -----------------------------------------------------------------------------
  {
    "https://github.com/L3MON4D3/LuaSnip.git",

    dependencies = { "rafamadriz/friendly-snippets" },

    -- Merge settings with LazyVim
    opts = {
      enable_autosnippets = true,
      updateEvents = "TextChanged,TextChangedI",
      history = false, -- Don't allow snippets to be jumped back into after they have been exited
    },

    init = function()
      -- Enable/load VS Code snippets
      require("luasnip.loaders.from_vscode").lazy_load()

      -- Enable Rails snippets in Ruby files
      require("luasnip").filetype_extend("ruby", { "rails" })
    end,
  },

  -----------------------------------------------------------------------------
  -- trouble - diagnostics enhancements
  -----------------------------------------------------------------------------
  {
    "https://github.com/folke/trouble.nvim.git",

    --  Disable all keymappings set by LazyVim
    keys = false,
  },

  -----------------------------------------------------------------------------
  -- vim-test - run tests in Neovim
  -----------------------------------------------------------------------------
  {
    "https://github.com/vim-test/vim-test.git",

    init = function()
      vim.cmd([[
        let test#strategy                    = 'neovim_sticky' " Run tests in Neovim's terminal in a split window
        let test#neovim_sticky#reopen_window = 1 " Reopen split window if not visible
        let test#neovim#term_position        = 'vertical'

        let g:test#echo_command       = 0 " Don't echo the test command before running it
        let g:test#preserve_screen    = 0 " Clear screen from previous run

        let test#ruby#rspec#options = '--format documentation'
      ]])
    end,
  },
}
