return {
  icons = {
    diagnostics = {
      Error = "🔥",
      Warn = "🔔",
      Hint = "🔰",
      Info = "🔷",
    },
  },
  filetypes_without_indent_guidelines = {
    "dashboard",
    "help",
    "lazy",
    "lazyterm",
    "mason",
    "notify",
    "NvimTree",
    "toggleterm",
    "Trouble",
    "trouble",
    "undotree",
  },
}
