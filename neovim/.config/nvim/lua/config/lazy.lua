local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local clone_lazyrepo_result =
    vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })

  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { clone_lazyrepo_result, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})

    vim.fn.getchar()
    os.exit(1)
  end
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  spec = {

    -- Setup LazyVim
    {
      "LazyVim/LazyVim",
      import = "lazyvim/plugins",
      opts = {
        colorscheme = "onedark_vivid",
        defaults = {
          keymaps = false, -- Disable LazyVim's default global keymappings
        },
        news = {
          lazyvim = true, -- Show LazyVim news
          neovim = true, -- Show Neovim news
        },
      },
    },

    -- Import all of the plugins defined in the plugins directory
    { import = "plugins" },
  },
  change_detection = {
    notify = false, -- Disable notifications when changes are detected
  },
  checker = {
    enabled = true, -- Automatically check for plugin updates
    notify = false, -- Disable notifications when plugin updates are found
  },
  install = { colorscheme = { "onedarkpro" } },
  performance = {
    rtp = {
      disabled_plugins = {
        "gzip",
        "netrwPlugin",
        "tarPlugin",
        "tohtml",
        "tutor",
        "zipPlugin",
      },
    },
  },
  ui = {
    border = "single",
  },
})
