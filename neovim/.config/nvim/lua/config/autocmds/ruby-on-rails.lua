vim.api.nvim_create_augroup("ruby_on_rails", { clear = true })

vim.api.nvim_create_autocmd({ "BufRead" }, {
  group = "ruby_on_rails",
  pattern = { "*.rb", "*.erb" },

  callback = function()
    -------------------------------------------------
    -- Setup keymappings to find Ruby on Rails files
    -------------------------------------------------

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>flc",
      function()
        Snacks.picker.files({
          finder = "files",
          format = "file",
          show_empty = true, -- Shows the picker even when there aren't any items to display
          supports_live = true, -- Live searching
          dirs = { "app/controllers" },
        })
      end,
      { noremap = true, silent = true, desc = "Find Rails controller" }
    )

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>flh",
      function()
        Snacks.picker.files({
          finder = "files",
          format = "file",
          show_empty = true, -- Shows the picker even when there aren't any items to display
          supports_live = true, -- Live searching
          dirs = { "app/helpers" },
        })
      end,
      { noremap = true, silent = true, desc = "Find Rails helper" }
    )

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>flm",
      function()
        Snacks.picker.files({
          finder = "files",
          format = "file",
          show_empty = true, -- Shows the picker even when there aren't any items to display
          supports_live = true, -- Live searching
          dirs = { "app/models" },
        })
      end,
      { noremap = true, silent = true, desc = "Find Rails model" }
    )

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>fls",
      function()
        Snacks.picker.files({
          finder = "files",
          format = "file",
          show_empty = true, -- Shows the picker even when there aren't any items to display
          supports_live = true, -- Live searching
          dirs = { "app/services" },
        })
      end,
      { noremap = true, silent = true, desc = "Find Rails service" }
    )

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>flv",
      function()
        Snacks.picker.files({
          finder = "files",
          format = "file",
          show_empty = true, -- Shows the picker even when there aren't any items to display
          supports_live = true, -- Live searching
          dirs = { "app/services" },
        })
      end,
      { noremap = true, silent = true, desc = "Find Rails view" }
    )

    -- stylua: ignore
    vim.keymap.set(
      "n",
      "<Leader>flg",
      ":e Gemfile<CR>",
      { noremap = true, silent = true, desc = "Open Gemfile" }
    )

    vim.keymap.set(
      "n",
      "<Leader>fll",
      ":Emigration<CR>",
      { noremap = true, silent = true, desc = "Open last migration" }
    )

    vim.keymap.set(
      "n",
      "<Leader>flr",
      ":e config/routes.rb<CR>",
      { noremap = true, silent = true, desc = "Open routes file" }
    )
  end,
})
