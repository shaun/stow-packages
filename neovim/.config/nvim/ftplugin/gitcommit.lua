local function format_paragraph()
  vim.opt_local.formatexpr = ""
  vim.cmd("silent normal! gqip")
end

-- Use par (if it's available) to do paragraph formatting
if vim.fn.executable("par") then
  vim.opt_local.formatprg = "par"
end

vim.keymap.set(
  { "i", "n" },
  "<C-l><C-f>",
  format_paragraph,
  { buffer = true, noremap = true, silent = true, desc = "Format paragraph, (l)ocal (f)ormat" }
)
